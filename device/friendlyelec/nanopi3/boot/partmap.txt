# sd0 partition map
# flash= <device>.<dev no>:<partition>:<fstype>:<start>,<length>
#   support device : eeprom, nand, mmc
#   support fstype : 2nd, boot, raw, fat, ext4, ubi
#
flash=mmc,0:2ndboot:2nd:0x200,0x10000:bl1-mmcboot.bin;
flash=mmc,0:fip-loader:boot:0x10200,0x50000:fip-loader.img;
flash=mmc,0:fip-secure:boot:0x60200,0x180000:fip-secure.img;
flash=mmc,0:fip-nonsecure:boot:0x1E0200,0x100000:fip-nonsecure.img;
flash=mmc,0:env:env:0x2E0200,0x4000;
flash=mmc,0:boot:ext4:0x400000,0x4000000:boot.img;
flash=mmc,0:system:ext4:0x4400000,0x80000000:system.img;
flash=mmc,0:vendor:ext4:0x84500000,0x4000000;
flash=mmc,0:cache:ext4:0x88600000,0x20000000:cache.img;
flash=mmc,0:misc:emmc:0xA8700000,0x400000;
flash=mmc,0:userdata:ext4:0xA8C00000,0:userdata.img;
