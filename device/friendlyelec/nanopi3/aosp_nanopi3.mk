#
# Copyright (C) 2015 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)

PRODUCT_PROPERTY_OVERRIDES += \
	ro.product.first_api_level=24

$(call inherit-product, device/friendlyelec/nanopi3/device.mk)

PRODUCT_PACKAGES += \
	Launcher3

# Discard inherited values and use our own instead.
PRODUCT_NAME := aosp_nanopi3
PRODUCT_DEVICE := nanopi3
PRODUCT_BRAND := Android
PRODUCT_MODEL := AOSP on NanoPi 3
PRODUCT_MANUFACTURER := FriendlyELEC (www.friendlyarm.com)

